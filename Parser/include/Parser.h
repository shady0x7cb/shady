#ifndef PARSER_H
#define PARSER_H

#include <bits/stdc++.h>

using namespace std;

class LexicalAnalyzer;

class Parser
{
    public:
        Parser(LexicalAnalyzer* _lexer, const string& _grammar_file, const string& _output_file);
        void init();
        void parse();
        virtual ~Parser();

    protected:

    private:
        LexicalAnalyzer* lexer;
        string grammar_file;
        string output_file;
};

#endif // PARSER_H
