#ifndef PARSERENGINE_H
#define PARSERENGINE_H
#include "ParsingTable.h"
#include <string>
#include <vector>
using namespace std;

class LexicalAnalyzer;

class ParserEngine
{
    public:
        ParserEngine(LexicalAnalyzer *la, ParsingTable* pt);
        virtual ~ParserEngine();
        void parse(string file_name);


    protected:

    private:
        LexicalAnalyzer* lexer;
        ParsingTable* parsing_table;
};

#endif // PARSERENGINE_H
