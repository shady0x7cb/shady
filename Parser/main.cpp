#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <regex>
#include <fstream>
#include "GrammerParser.h"

using namespace std;

int main()
{


    //freopen("out.txt", "w", stdout);
    GrammerParser gp;

    vector<GrammerParser::NonTerminal> v = gp.get_grammer();



    gp.eliminate_left_recursion();
    //gp.eliminate_left_factoring();
    vector<GrammerParser::NonTerminal> d = gp.replaced;

    for(int i = 0 ; i < (int)d.size() ; i++){
        vector<vector<int>> g = d[i].rule;
        for(int j = 0 ; j < (int)g.size() ; j++){
            for(int h = 0 ; h < (int)g[j].size() ; h++){
                cout << g[j][h] << ", ";
            }
            cout << " - ";
        }
        cout << "\n";
    }

    gp.print_map();


    return 0;
}
