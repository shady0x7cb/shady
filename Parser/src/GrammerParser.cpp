#include "GrammerParser.h"
#include <iostream>
#include <vector>
#include <string>
#include <regex>
#include <fstream>


GrammerParser::GrammerParser()
{
    //ctor
}

GrammerParser::~GrammerParser()
{
    //dtor
}

int GrammerParser::getSymCount()
{
    return 0;
}

string GrammerParser::remove_end_spaces(const string &s)
{
    int last = s.size() - 1;
    while (last >= 0 && (s[last] == ' ' || s[last] == '\t'))
      --last;
    return s.substr(0, last + 1);
}

int GrammerParser::symbol_to_number(const string& symbol)
{
    return string_to_number1[symbol];
}

bool GrammerParser::isterminal(int symbol)
{
    return terminal[symbol];
}


vector<GrammerParser::NonTerminal> GrammerParser::to_LL1()
{
    //eliminate_left_recursion();

    return grammer;
}

string GrammerParser::number_to_symbol(int number)
{
    return number_to_string1[number];
}

vector<GrammerParser::NonTerminal> GrammerParser::get_grammer()
{
    read("");
    vector<GrammerParser::NonTerminal> number_grammer;
    GrammerParser::NonTerminal non_term;
    vector<vector<int>> vec;
    vector<int> v;
    for(int i = 0 ; i < (int)gram.size() ; i++){
        std::istringstream buf(gram[i]);
        std::istream_iterator<std::string> beg(buf), end;
        std::vector<std::string> tokens(beg, end);
        for(string s : tokens){
            if(s == "|")
            {
                vec.push_back(v);
                v.clear();
            }
            else
            {
                int thevalue;
                std::istringstream ss(s);
                ss >> thevalue;
                v.push_back(thevalue);

            }
        }
        if(v.size() > 0)
        {
            vec.push_back(v);
            v.clear();
        }
        non_term.rule = vec;

        number_grammer.push_back(non_term);


        vec.clear();
    }

    grammer = number_grammer;
    replaced = grammer;
    return grammer;
}

void GrammerParser::replace_string(std::string& subject, const std::string& search, const string& replace)
{
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != string::npos) {
         if(search != "\'" && (pos > 0 && subject.at(pos-1)!=' ') /*|| (pos < subject.size()-1  && subject.at(pos+1)!=' ') */){
            break;
         }
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}

int GrammerParser::fill_maps_terminals(string g, int counter)
{
    std::map<string,int>::iterator it;
    string f = "";
    int c = counter;
    for(size_t i = 0 ; i < g.size(); i++){
        if(g.at(i) == '\''){
            f += g.at(i);
            i++;
            while(g.at(i) != '\''){
                f += g.at(i);
                i++;
            }
            f += g.at(i);
            it = string_to_number.find(f);
            if(!(it != string_to_number.end())){
                string_to_number.insert (std::pair<string,int>((f), c));
                number_to_string.insert (std::pair<int,string>(c, (f)));
                terminal.insert (std::pair<int,bool>(c, true));
                c++;
            }
            f = "";
        }
    }
    if(c > counter){
        return c;
    }else{
        return counter;
    }
}
void GrammerParser::read(string file_name)
{
    int counter = 0;
    string temp = "", input = "";
    regex rgx("\\s*(#)\\s*(.[^=]+)\\s*(=)\\s*(.+)\\s*");
    std::smatch matches;
    ifstream file ("file.txt");
    if(file.is_open()){
        while(getline(file, input))
        {
            if(regex_match(input, rgx))
            {
                if(temp != "")
                {
                    if(std::regex_search(temp, matches, rgx))
                    {
                        string_to_number.insert (std::pair<string,int>(remove_end_spaces(matches[2].str()),counter));
                        number_to_string.insert (std::pair<int,string>(counter, remove_end_spaces(matches[2].str())));
                        terminal.insert (std::pair<int,bool>(counter, false));
                        counter++;
                    }
                    else
                    {
                    }
                    gram.push_back(matches[4].str());
                    temp = "";
                }
                temp += input;
            }
            else
            {
                temp += input;
                temp += " ";
            }
        }
        if(temp != ""){
            if(std::regex_search(temp, matches, rgx))
            {
                string_to_number.insert (std::pair<string,int>(remove_end_spaces(matches[2].str()),counter));
                number_to_string.insert (std::pair<int,string>(counter, remove_end_spaces(matches[2].str())));
                counter++;
            }
            else
            {
                //std::cout << "Match not found\n";
            }
            gram.push_back(matches[4].str());
        }
        file.close();

        for(int i = 0 ; i < (int)gram.size() ; i++){
            counter = fill_maps_terminals(gram[i], counter);
        }
        std::map<int,string>::iterator it;
        for(int i = 0 ; i < (int)gram.size() ; i++){
            for (it=number_to_string.begin(); it!=number_to_string.end(); ++it){
                string myint_str =' ' +  static_cast<ostringstream*>( &(ostringstream() << it->first) )->str() + ' ';
                replace_string(gram[i], it->second, myint_str);
            }

        }

    }
}



void GrammerParser::print_map()
{
    std::map<string,int>::iterator it = string_to_number1.begin();
    std::cout << "string to int contains:\n";
    for (it=string_to_number1.begin(); it!=string_to_number1.end(); ++it)
        std::cout << it->first << "=>" << it->second << '\n';

    std::map<int,string>::iterator it1 = number_to_string1.begin();
    std::cout << "int to string contains:\n";
    for (it1=number_to_string1.begin(); it1!=number_to_string1.end(); ++it1)
        std::cout << it1->first << "=>" << it1->second << '\n';
}



void GrammerParser::replace_rules(int i, int c, int j)
{

    vector<int> g = replaced[i].rule[c];
    vector<int> after;
    for(int z = 1 ; z < (int)g.size() ; z++){
        after.push_back(g[i]);
    }
    vector<vector<int>> v = replaced[j].rule;
    if(after.size() > 0){
        for(int k = 0 ; k < (int)replaced[j].rule.size() ; k++){
            for(int u = 0 ; u < (int)after.size() ; u++){
                v[k].push_back(after[u]);
            }
        }
    }
    replaced[i].rule.erase(replaced[i].rule.begin()+c);
    for(int x = 0 ; x < (int)v.size() ; x++){
        replaced[i].rule.insert(replaced[i].rule.begin() + c + x, v[x]);
    }


}

void GrammerParser::eliminate_immediate_left_recursion(int i)
{
    vector<vector<int>> beta;
    vector<vector<int>> alpha;
    vector<int> temp_alpha;
    vector<int> temp_beta;
    vector<int> o = contain(i, i);
    if(o.size() > 0){

        new_name.push_back(number_to_string[i] + "'");
        for(int c = 0 ; c < (int)o.size() ; c++){
            for(int j = 1 ; j < (int)replaced[i].rule[o[c]].size() ; j++){
                temp_alpha.push_back(replaced[i].rule[o[c]][j]);
            }
            if(temp_alpha.size() > 0){
                alpha.push_back(temp_alpha);
                temp_alpha.clear();
            }
        }
        vector<int> b;
        for(int j = 0 ; j < (int)replaced[i].rule.size() ; j++){
            if(replaced[i].rule[j][0] != i){
                b.push_back(j);
            }
        }
        for(int j = 0 ; j < (int)b.size() ; j++){
            for(int h = 0 ; h < (int)replaced[i].rule[b[j]].size() ; h++){
                temp_beta.push_back(replaced[i].rule[b[j]][h]);
            }
            if(temp_beta.size() > 0){
                beta.push_back(temp_beta);
                temp_beta.clear();
            }
        }
        /*cout << "alpha\n" <<endl;
        for(int y = 0 ; y < alpha.size(); y++){
            for(int s = 0 ; s < alpha[y].size() ;s++){
                cout << alpha[y][s] <<", " ;
            }
            cout << "\n";
        }
        cout << "alpha\n" <<endl;
        cout << "beta\n" <<endl;
        for(int y = 0 ; y < beta.size(); y++){
            for(int s = 0 ; s < beta[y].size() ;s++){
                cout << beta[y][s] <<", " ;
            }
            cout << "\n";
        }
        cout << "beta\n" <<endl;
        */
        int x = replaced.size() + added.size();
        adjusted.push_back(i);
        //number_to_string[]
        //cout << x<<"\n";
        if(alpha.size() > 0){

            for(int j = 0 ; j < (int)alpha.size();  j++){
                alpha[j].push_back(x);
            }
        }
        /*cout << "alpha\n" <<endl;
        for(int y = 0 ; y < alpha.size(); y++){
            for(int s = 0 ; s < alpha[y].size() ;s++){
                cout << alpha[y][s] <<", " ;
            }
            cout << "\n";
        }
        cout << "alpha\n" <<endl;*/
        if(beta.size() > 0){
            for(int j = 0 ; j < (int)beta.size();  j++){
                beta[j].push_back(x);
            }
            replaced[i].rule = beta;
        }

        if(alpha.size() > 0){
            GrammerParser::NonTerminal non;
            non.rule = alpha;
            added.push_back(non);
            left_rec.push_back(x);
        }

    }
    /*cout << "alpha\n" <<endl;
    for(int y = 0 ; y < alpha.size(); y++){
        for(int s = 0 ; s < alpha[y].size() ;s++){
            cout << alpha[y][s] <<", " ;
        }
        cout << "\n";
    }
    cout << "alpha\n" <<endl;*/








}
vector<int> GrammerParser::contain(int i, int j)
{

    vector<vector<int>> v = grammer[i].rule;
    vector<int> d;
    for(int k = 0 ; k < (int)v.size() ; k++){
        if(v[k][0] == j){
            d.push_back(k);
        }
    }
    return d;
}


void GrammerParser::eliminate_left_recursion()
{
    for(int i = 0 ; i < (int)gram.size() ; i++){
        for(int j = 0 ; j < i ; j++){
            vector<int> r = contain(i, j);
            if(r.size() > 0){
                for(int c = 0 ; c < (int)r.size() ; c++){
                    replace_rules(i, r[c], j);
                }
            }
        }
        eliminate_immediate_left_recursion(i);
    }
    vector<int> k;

    int u = string_to_number.size() + added.size();
    k.push_back(u);
    string_to_number["\\L"] = u;
    number_to_string[u] = "\\L";
    if(added.size() > 0){
        for(int i = 0 ; i < (int)added.size() ; i++){
            added[i].rule.push_back(k);
            replaced.push_back(added[i]);
        }
    }
    for(int i = 0 ; i < (int)new_name.size() ; i++){
        cout << new_name[i] << ", ";
    }
    map<string, int>::iterator it = string_to_number.begin();

    for(it = string_to_number.begin() ; it != string_to_number.end() ; ++it){
        if(it->second >=(int)gram.size()){
            it->second += added.size();
        }

    }
    int num = gram.size();
    for(int i = 0 ; i < (int)new_name.size() ; i++){
        terminal.insert(std::pair<int, bool>(num, false));
        string_to_number[new_name[i]] = num++;
    }

    map<string, int>::iterator it1 = string_to_number.begin();

    for(it1 = string_to_number.begin() ; it1 != string_to_number.end() ; ++it1){
        string temp = it1->first;
        if(it1->second >= (int)gram.size() + (int)added.size()){
            replace_string(temp, "'", "");
        }
        string_to_number1.insert(std::pair<string, int>(temp, it1->second));
        number_to_string1.insert(std::pair<int, string>(it1->second, temp));
    }
    grammer = replaced;


}
bool GrammerParser::cont(int num, vector<int> v){
    for(int i = 0 ; i < v.size() ; i++){
        if(v[i] == num){
            return true;
        }
    }
    return false;
}
void GrammerParser::eliminate_factoring(int num, vector<int> vec)
{
    vector<vector<int>> alpha;
    vector<int> alpha_temp;
    vector<vector<int>> beta;
    vector<int> beta_temp;
    int number_of_equal_elements = 0;
    vector<vector<int>> v = replaced[num].rule;

    int u = 0;
    bool t = true;
    while(t){
        int u = v[vec[0]][number_of_equal_elements];
        for(int i = 0 ; i < vec.size() ; i++){
            if(v[vec[i]][number_of_equal_elements] != u){
                t = false;
            }
        }
        number_of_equal_elements++;
    }

    for(int i = 0 ; i < number_of_equal_elements ; i++){
        alpha_temp.push_back(v[vec[0]][i]);
    }
    int x = replaced.size();
    alpha_temp.push_back(x);
    alpha.push_back(alpha_temp);
    alpha_temp.clear();
    for(int i = 0 ; (i < (int)v.size()) && (!cont(i, vec)) ; i++){
        for(int j = 0 ; j < v[i].size() ; j++){
            alpha_temp.push_back(v[i][j]);
        }
        alpha.push_back(alpha_temp);
        alpha_temp.clear();
    }
    replaced[num].rule = alpha;

    for(int i = 0 ; i < (int)vec.size() ; i++){
        for(int j = 0 ; j < number_of_equal_elements ; j++){
            //cout <<"shady, " <<num;
            if(v[vec[i]].size() > 0){
                v[vec[i]].erase(v[vec[i]].begin());
            }
        }

    }
    cout <<"shady, " <<num;
    int k = 0;
    for(int i = 0 ; i < v.size() && (!cont(i, vec)); i++){
        if(v.size() > 0){
            v.erase(v.begin() + i + k);
        }
        k++;
    }
    GrammerParser::NonTerminal n;
    n.rule = v;
    replaced.push_back(n);

}
vector<int> GrammerParser::get_vector(int num){
    vector<int> vec;
    vector<vector<int>> v = replaced[num].rule;
    for(int i = 0 ; i < (int)v.size() ; i++){
        if( (i+1 < (int)v.size()) && (v[i][0] == v[i+1][0]) ){
            if(!cont(i, vec)){
                vec.push_back(i);
            }
            if(!cont(i+1, vec)){
                vec.push_back(i+1);
            }

        }
    }
    return vec;
}
void GrammerParser::eliminate_left_factoring()
{
    for(int i = 0 ; i < (int)replaced.size() ; i++){
        vector<int> v = get_vector(i);
        if(v.size() > 1){
            cout << "yes" <<endl;
            eliminate_factoring(i, v);
        }
    }
}


